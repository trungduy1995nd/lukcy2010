import { Logger, Module } from '@nestjs/common';
import { LuckyModule } from './lucky/lucky.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { MaillerModule } from './mailler/mailler.module';
import { RoleModule } from './role/role.module';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './auth/guards/roles.guard';
import { ThrottlerGuard } from '@nestjs/throttler';

@Module({
	imports: [
		TypeOrmModule.forRoot(),
		ConfigModule.forRoot({
			envFilePath: '.env',
			isGlobal: true,
		}),
		MaillerModule,
		RoleModule,
		LuckyModule,
		UserModule,
		AuthModule,
	],
	providers: [
		Logger,
		{
			provide: APP_GUARD,
			useClass: RolesGuard,
		},
		{
			provide: APP_GUARD,
			useClass: ThrottlerGuard,
		},
	],
})
export class AppModule {}
