import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateLuckyDto } from './dto/create-lucky.dto';
import { UpdateLuckyDto } from './dto/update-lucky.dto';
import { LuckyHistory } from './entities/lucky-history.entity';
import { Lucky } from './entities/lucky.entity';

@Injectable()
export class LuckyService {
	constructor(
		@InjectRepository(Lucky) private luckyRepo: Repository<Lucky>,
		@InjectRepository(LuckyHistory)
		private lhRepo: Repository<LuckyHistory>,
	) {}

	findAllLucky() {
		return this.luckyRepo.find({ isActive: true });
	}

	saveGift(gift, user) {
		return this.lhRepo.save({ amount: 1, user, gift });
	}
}
