import {
	Column,
	CreateDateColumn,
	Entity,
	OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import { LuckyHistory } from './lucky-history.entity';

@Entity()
export class Lucky {
	@PrimaryGeneratedColumn({ unsigned: true })
	id: number;

	@Column()
	name: string;

	@Column()
	image: string;

	@Column()
	rate: number;

	@Column()
	total: number;

	@Column({ type: 'bigint', unsigned: true })
	price: number;

	@Column()
	isActive: boolean;

	@OneToMany(() => LuckyHistory, (lh) => lh.gift)
	giftHistories: LuckyHistory[];

	@CreateDateColumn()
	readonly createdAt: Date;

	@UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
	readonly updatedAt: Date;
}
