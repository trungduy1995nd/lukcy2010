import { User } from 'src/user/entities/user.entity';
import {
	Column,
	CreateDateColumn,
	Entity,
	ManyToMany,
	ManyToOne,
	OneToOne,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import { Lucky } from './lucky.entity';

@Entity()
export class LuckyHistory {
	@PrimaryGeneratedColumn({ unsigned: true })
	id: number;

	@ManyToOne(() => User, (user) => user.luckyHistories)
	user: User;

	@ManyToOne(() => Lucky, (gift) => gift.giftHistories)
	gift: Lucky;

	@Column()
	amount: number;

	@CreateDateColumn()
	readonly createdAt: Date;

	@UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
	readonly updatedAt: Date;
}
