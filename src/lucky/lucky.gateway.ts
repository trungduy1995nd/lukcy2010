import {
	MessageBody,
	SubscribeMessage,
	WebSocketGateway,
	WebSocketServer,
} from '@nestjs/websockets';
import { Server } from 'socket.io';

import { LuckyService } from './lucky.service';

@WebSocketGateway({ cors: { origin: '*' } })
export class LuckyGateway {
	constructor(private readonly luckyService: LuckyService) {}

	@WebSocketServer()
	server: Server;

	@SubscribeMessage('message')
	handleMessage(@MessageBody() body): void {
		try {
			if (body.key === 'lukcyResult') {
				this.server.emit('message', body);
			}
		} catch (err) {
			//
		}
	}
}
