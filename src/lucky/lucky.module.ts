import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from 'src/user/user.module';

import { LuckyHistory } from './entities/lucky-history.entity';
import { Lucky } from './entities/lucky.entity';
import { LuckyController } from './lucky.controller';
import { LuckyGateway } from './lucky.gateway';
import { LuckyService } from './lucky.service';

@Module({
	providers: [LuckyGateway, LuckyService],
	controllers: [LuckyController],
	imports: [TypeOrmModule.forFeature([Lucky, LuckyHistory]), UserModule],
})
export class LuckyModule {}
