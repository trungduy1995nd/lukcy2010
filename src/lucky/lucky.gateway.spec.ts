import { Test, TestingModule } from '@nestjs/testing';
import { LuckyGateway } from './lucky.gateway';
import { LuckyService } from './lucky.service';

describe('LuckyGateway', () => {
	let gateway: LuckyGateway;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [LuckyGateway, LuckyService],
		}).compile();

		gateway = module.get<LuckyGateway>(LuckyGateway);
	});

	it('should be defined', () => {
		expect(gateway).toBeDefined();
	});
});
