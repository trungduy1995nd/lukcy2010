import { Controller, Get, Post } from '@nestjs/common';
import { CurrentUser, Roles } from 'src/auth/auth.meta';
import { ResponseDto } from 'src/common/response.dto';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/user.service';

import { Lucky } from './entities/lucky.entity';
import { LuckyGateway } from './lucky.gateway';
import { LuckyService } from './lucky.service';

@Controller()
export class LuckyController {
	constructor(
		private readonly luckyService: LuckyService,
		private readonly userService: UserService,
		private readonly luckyGateway: LuckyGateway,
	) {}

	@Roles('MEMBER')
	@Post('spin')
	async spin(@CurrentUser() user: User): Promise<ResponseDto<Lucky>> {
		const gifts = await this.luckyService.findAllLucky();

		if (user.count < 1) {
			return {
				statusCode: 400,
				message: 'Không đủ lượt quay',
			};
		}

		const total = gifts.reduce((a, b) => a + (b['rate'] || 0), 0);
		const keepWeight = Math.random() * total;
		let totalWeight = 0;
		let result: Lucky;

		for (let i = 0; i < gifts.length; i++) {
			totalWeight += gifts[i].rate;
			if (totalWeight >= keepWeight) {
				result = gifts[i];
				break;
			}
		}
		const decrease = await this.userService.decreaseCount(user.id, 1);
		const history = await this.luckyService.saveGift(result, user);
		if (history) {
			setTimeout(() => {
				this.luckyGateway.server.emit('message', {
					key: 'lukcyResult',
					username: user.username,
					message: result.name,
				});
			}, 5000);
			return {
				statusCode: 200,
				data: result,
			};
		}
		return {
			statusCode: 400,
		};
	}

	@Get('gifts')
	async gifts(): Promise<ResponseDto<Lucky[]>> {
		const gifts = await this.luckyService.findAllLucky();

		if (gifts.length) {
			return {
				statusCode: 200,
				data: gifts,
			};
		}
		return {
			statusCode: 400,
		};
	}
}
