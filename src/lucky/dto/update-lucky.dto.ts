import { PartialType } from '@nestjs/mapped-types';
import { CreateLuckyDto } from './create-lucky.dto';

export class UpdateLuckyDto extends PartialType(CreateLuckyDto) {
  id: number;
}
