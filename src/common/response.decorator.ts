import { applyDecorators, Type } from '@nestjs/common';
import { ApiExtraModels, ApiResponse, getSchemaPath } from '@nestjs/swagger';

import { ResponsePaginatedDto, ResponseDto } from './response.dto';

export const ApiPaginatedResponse = <TModel extends Type<any>>(model: TModel) => {
	return applyDecorators(
		ApiExtraModels(ResponseDto, model),
		ApiResponse({
			status: 201,
			description: 'Success',
			schema: {
				allOf: [
					{ $ref: getSchemaPath(ResponsePaginatedDto) },
					{
						properties: {
							data: {
								type: 'array',
								items: { $ref: getSchemaPath(model) },
							},
						},
					},
				],
			},
		}),
	);
};

export const ApiDataResponse = <TModel extends Type<any>>(model: TModel) => {
	return applyDecorators(
		ApiExtraModels(ResponseDto, model),
		ApiResponse({
			status: 201,
			description: 'Success',
			schema: {
				title: `${model.name}`,
				allOf: [
					{ $ref: getSchemaPath(ResponseDto) },
					{
						properties: {
							data: { $ref: getSchemaPath(model) },
						},
					},
				],
			},
		}),
	);
};
