export interface Paginate<T = any> {
	data?: T;
	page?: number;
	total?: number;
	perPage?: number;
}
