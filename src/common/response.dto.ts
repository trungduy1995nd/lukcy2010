import { ApiProperty } from '@nestjs/swagger';

export class ResponseDto<TData = any> {
	@ApiProperty()
	data?: TData;

	@ApiProperty()
	message?: string | string[];

	@ApiProperty()
	error?: string;

	@ApiProperty()
	page?: number;

	@ApiProperty()
	perPage?: number;

	@ApiProperty()
	total?: number;

	@ApiProperty()
	statusCode = 200;
}

export class ResponsePaginatedDto<TData> {
	@ApiProperty()
	statusCode: number;

	@ApiProperty()
	message: string;

	@ApiProperty()
	total: number;

	@ApiProperty()
	page: number;

	@ApiProperty()
	perPage: number;

	data: TData[];
}
