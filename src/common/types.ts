/* eslint-disable @typescript-eslint/ban-types */
export type PrimitiveKeys<T> = {
	[P in keyof T]: Exclude<T[P], undefined> extends object ? never : P;
}[keyof T];
export type Only<T> = Pick<T, PrimitiveKeys<T>>;
