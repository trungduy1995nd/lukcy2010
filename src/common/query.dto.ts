import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumberString, IsOptional, IsString } from 'class-validator';

export class FindQueryDto {
	@ApiProperty({ default: 1 })
	@IsNumberString()
	page: number;

	@ApiProperty({ default: 10 })
	@IsNumberString()
	perPage: number;

	@ApiPropertyOptional()
	@IsString()
	@IsOptional()
	search?: string;
}
