export const getRandomInt = (min: number, max: number): number => {
	return Math.floor(Math.random() * (max - min) + min);
};

interface Pagination {
	skip: number;
	take: number;
	order: any;
}

export const makePagination = ({ page, perPage, order = 'DESC', orderBy = 'id' }): Pagination => {
	if (page < 1) page = 1;
	if (perPage < 1) perPage = 10;
	const skip = Number((page - 1) * +perPage);
	const take = Number(perPage);
	const sortBy = { [`${orderBy}`]: order };
	return { skip, take, order: sortBy };
};
