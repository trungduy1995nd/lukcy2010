export const can = (requireRoles, roles) => {
	const listRoles = [];
	let isSuperAdmin = false;
	roles.map((role) => {
		if (role.name === 'ADMINISTRATOR') {
			isSuperAdmin = true;
		}
		listRoles.push(role.name);
	});
	if (isSuperAdmin) {
		return true;
	}
	return requireRoles.some((rr) => {
		if (listRoles.indexOf(rr) !== -1) {
			return true;
		}
	});
};
