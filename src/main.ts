import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { join } from 'path';

import { AppModule } from './app.module';

async function bootstrap() {
	const app = await NestFactory.create<NestExpressApplication>(AppModule, {
		bodyParser: true,
		// cors: true,
	});

	app.enableCors();
	app.setGlobalPrefix('/api/v1');

	app.useGlobalPipes(
		new ValidationPipe({
			whitelist: true,
			transform: true,
		}),
	);
	app.useStaticAssets(join(__dirname, '..', 'public'));
	if (process.env.NODE_ENV !== 'production') {
		const config = new DocumentBuilder()
			.setTitle('App Docs')
			.setDescription('The API description')
			.setVersion('1.0')
			.addBearerAuth()
			.build();
		const document = SwaggerModule.createDocument(app, config);
		SwaggerModule.setup('docs', app, document, {
			swaggerOptions: {
				persistAuthorization: true,
				filter: true,
			},
		});
	}
	await app.listen(process.env.PORT || 3000);
}
bootstrap();
