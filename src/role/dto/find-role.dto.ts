import { FindQueryDto } from 'src/common/query.dto';

export class FindRoleDto extends FindQueryDto {}
