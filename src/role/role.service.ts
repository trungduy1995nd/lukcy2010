import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Paginate } from 'src/common/api.interface';
import { makePagination } from 'src/utils/libs';
import { Repository } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';
import { FindRoleDto } from './dto/find-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Permission, Role } from './entities/role.entity';

@Injectable()
export class RoleService {
	constructor(@InjectRepository(Role) private roleRepo: Repository<Role>, @InjectRepository(Permission) private perRepo: Repository<Permission>) {}

	async findOne(id) {
		return this.roleRepo.findOne(id);
	}

	async findByName(name: string) {
		return this.roleRepo.find({ name });
	}

	async create(createRoleDto: CreateRoleDto) {
		return this.roleRepo.create(createRoleDto);
	}

	async find() {
		return this.roleRepo.find();
	}

	async findAll(query: FindRoleDto): Promise<Paginate> {
		const { skip, order, take } = makePagination(query);

		const [result, total] = await this.roleRepo.findAndCount({
			order: order,
			take: take,
			skip: skip,
		});

		return {
			data: result,
			total: total,
		};
	}

	async update(id, updateRoleDto: UpdateRoleDto) {
		return this.roleRepo.update(id, updateRoleDto);
	}

	async remove(id) {
		return this.roleRepo.remove(id);
	}
}
