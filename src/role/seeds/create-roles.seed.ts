import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';
import { Role } from '../entities/role.entity';

export default class CreateRoles implements Seeder {
	public async run(factory: Factory, connection: Connection): Promise<any> {
		await connection
			.createQueryBuilder()
			.insert()
			.into(Role)
			.values([
				{
					name: 'ADMINISTRATOR',
					description: 'Quyền Admin cao nhất',
					active: true,
				},
				{
					name: 'MEMBER',
					description: 'Quyền thành viên hệ thống',
					active: true,
				},
			])
			.execute();
	}
}
