import { User } from 'src/user/entities/user.entity';
import { Column, CreateDateColumn, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class Role {
	@PrimaryGeneratedColumn({ unsigned: true })
	id: number;

	@Column({ unique: true })
	name: string;

	@Column()
	description: string;

	@Column()
	active: boolean;

	@ManyToMany(() => Permission, (perm) => perm.role)
	@JoinTable({ name: 'roles_permissions' })
	permissions: Permission[];

	@OneToMany(() => User, (user) => user.roles)
	user: User;

	@CreateDateColumn()
	readonly createdAt: Date;

	@UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
	readonly updatedAt: Date;
}

@Entity()
export class Permission {
	@PrimaryGeneratedColumn({ unsigned: true })
	id: number;

	@Column({ unique: true })
	name: string;

	@Column({ unique: true })
	slug: string;

	@Column()
	active: boolean;

	@OneToMany(() => Role, (role) => role.permissions)
	role: Role;

	@CreateDateColumn()
	readonly createdAt: Date;

	@UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
	readonly updatedAt: Date;
}
