import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Permission, Role } from './entities/role.entity';
import { RoleService } from './role.service';
import { RoleController } from './role.controller';

@Global()
@Module({
	providers: [RoleService],
	imports: [TypeOrmModule.forFeature([Role, Permission])],
	exports: [RoleService],
	controllers: [RoleController],
})
export class RoleModule {}
