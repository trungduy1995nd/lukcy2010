import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/auth.meta';
import { I18N } from 'src/utils/lang';
import { CreateRoleDto } from './dto/create-role.dto';
import { FindRoleDto } from './dto/find-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { RoleService } from './role.service';

@ApiTags('Role')
@Controller('role')
export class RoleController {
	constructor(private readonly roleService: RoleService) {}

	@Post()
	@Roles('CREATE_ROLE')
	create(@Body() createRoleDto: CreateRoleDto) {
		return this.roleService.create(createRoleDto);
	}

	@Get()
	@Roles('READ_ROLE')
	async find(@Query() query: FindRoleDto) {
		try {
			const result = await this.roleService.findAll(query);
			if (result.total) {
				return {
					statusCode: 200,
					...result,
					page: query.page,
					perPage: query.perPage,
				};
			} else {
				return {
					message: I18N('Không tìm thấy dữ liệu'),
					statusCode: 404,
					data: [],
				};
			}
		} catch (error) {
			return {
				message: I18N('Không tìm thấy dữ liệu'),
				statusCode: 400,
				error: error.code,
			};
		}
	}

	@Get(':id')
	@Roles('READ_ROLE')
	findOne(@Param('id') id: string) {
		return this.roleService.findOne(+id);
	}

	@Patch(':id')
	@Roles('UPDATE_ROLE')
	update(@Param('id') id: string, @Body() updateRoleDto: UpdateRoleDto) {
		return this.roleService.update(+id, updateRoleDto);
	}

	@Delete(':id')
	@Roles('DELETE_ROLE')
	remove(@Param('id') id: string) {
		return this.roleService.remove(+id);
	}
}
