import { Global, Module } from '@nestjs/common';
import { UserModule } from 'src/user/user.module';
import { MaillerService } from './mailler.service';

@Global()
@Module({
	providers: [MaillerService],
	exports: [MaillerService],
	imports: [UserModule],
})
export class MaillerModule {}
