import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { createTransport, Transporter } from 'nodemailer';
import { ResponseDto } from 'src/common/response.dto';
import { UserService } from 'src/user/user.service';
import { I18N } from 'src/utils/lang';
import forgotTemplate from './templates/forgot';

@Injectable()
export class MaillerService {
	private transporter: Transporter;

	constructor(
		private configService: ConfigService,
		private userService: UserService,
	) {
		const host = configService.get('SMTP_HOST');
		const user = configService.get('SMTP_USER');
		const password = configService.get('SMTP_PASS');
		this.transporter = createTransport({
			host: host,
			port: 587,
			secure: false,
			auth: {
				user: user,
				pass: password,
			},
		});
	}

	async sendMail({ to, subject, content }) {
		return this.transporter.sendMail({
			from: 'Thiên Kích <thienkich.com@gmail.com>',
			to: to,
			subject: subject,
			html: content,
		});
	}

	async sendMailForgot(to): Promise<ResponseDto> {
		const user = await this.userService.generateForgotToken(to);
		if (user && user.resetPasswordToken) {
			const url = `https://thienkich.com/auth/reset-password/?token=${user.resetPasswordToken}&email=${user.email}`;
			const mail = await this.sendMail({
				to,
				subject: I18N('Khôi phục mật khẩu | Thiên Kích'),
				content: forgotTemplate(url),
			});
			if (mail) {
				return {
					statusCode: 200,

					message: I18N('Thông tin đã được gửi về email của bạn'),
				};
			}
		}

		return {
			statusCode: 400,
			message: I18N('Không tìm thấy tài khoản tương ứng với email'),
		};
	}
}
