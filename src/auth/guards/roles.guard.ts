import {
	CanActivate,
	ExecutionContext,
	Injectable,
	UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { can } from 'src/utils/helpers/role.helpers';

@Injectable()
export class RolesGuard extends AuthGuard('jwt') implements CanActivate {
	constructor(private reflector: Reflector) {
		super();
	}

	/**
	 * Check user isLoggedin and user can do something base on parameter roles
	 *
	 * @param {ExecutionContext} context
	 * @returns {Promise<boolean>}
	 * @memberof RolesGuard
	 */
	async canActivate(context: ExecutionContext): Promise<boolean> {
		const roles = this.reflector.get<string[]>(
			'roles',
			context.getHandler(),
		);

		if (!roles) {
			return true;
		}

		const isAuth = await super.canActivate(context);

		if (!isAuth) {
			return false;
		}

		const request = context.switchToHttp().getRequest();
		const { user } = request;
		const isValidRole = can(roles, user.roles);

		if (isValidRole) {
			return true;
		}

		return false;
	}

	/**
	 * Handle error code if canActivate return false
	 *
	 * @param {*} err null
	 * @param {*} user
	 * @param {*} info | TokenExpiredError | JsonWebTokenError
	 * @returns
	 * @memberof RolesGuard
	 */
	handleRequest(err, user) {
		if (err || !user) {
			throw err || new UnauthorizedException();
		}
		return user;
	}
}
