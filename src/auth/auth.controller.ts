import { HttpService } from '@nestjs/axios';
import { Body, Controller, Logger, Post } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ApiTags } from '@nestjs/swagger';
import { Throttle } from '@nestjs/throttler';
import * as bcrypt from 'bcrypt';
import * as dayjs from 'dayjs';
import { ApiDataResponse } from 'src/common/response.decorator';
import { ResponseDto } from 'src/common/response.dto';
import { MaillerService } from 'src/mailler/mailler.service';
import { RoleService } from 'src/role/role.service';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/user.service';
import { I18N } from 'src/utils/lang';

import {
	ForgotBody,
	LoginBody,
	LoginResponseDto,
	RegisterBody,
	ResetBody,
	SendOtpBody,
	ValidateBody,
	VerifyGoogleTokenBody,
	VerifyOtpBody,
} from './auth.dto';
import { AuthService } from './auth.service';

// eslint-disable-next-line @typescript-eslint/no-var-requires

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
	private readonly logger = new Logger(AuthController.name);
	constructor(
		private readonly userService: UserService,
		private readonly jwtService: JwtService,
		private readonly mailler: MaillerService,
		private readonly httpService: HttpService,
		private readonly roleService: RoleService,
		private readonly authService: AuthService,
	) {}

	@Post('login')
	@ApiDataResponse(LoginResponseDto)
	async login(
		@Body() body: LoginBody,
	): Promise<ResponseDto<LoginResponseDto>> {
		try {
			const { username, password } = body;
			const user = await this.userService.validateUsername(username);
			if (!user) {
				return {
					statusCode: 400,
					message: I18N('Tài khoản không tồn tại'),
				};
			}

			const isValidPassword = true;
			// const isValidPassword = await bcrypt.compare(
			// 	password,
			// 	user.password,
			// );
			if (isValidPassword) {
				const payload = {
					username: user.username,
					phone: user.phone,
				};

				return {
					data: {
						token: this.jwtService.sign(payload),
						username: user.username,
						expiredAt: dayjs().add(30, 'days').unix(),
						email: user.email,
						phone: user.phone,
						address: user.address,
					},
					statusCode: 200,
					message: I18N('Đăng nhập thành công'),
				};
			} else {
				return {
					statusCode: 400,
					message: I18N('Mật khẩu không chính xác'),
				};
			}
		} catch (err) {
			console.log(err);
			return {
				statusCode: 400,
				message: I18N('Không thể đăng nhập'),
			};
		}
	}

	@Post('register')
	async register(@Body() body: RegisterBody): Promise<ResponseDto> {
		const user = new User();
		Object.assign(user, body);

		user.password = await bcrypt.hash(user.password, 10);
		user.phone = body.phone.toLowerCase();
		user.email = body.email.toLowerCase();
		user.roles = await this.roleService.findByName('MEMBER');

		try {
			const isEmailExist = await this.userService.exists({
				key: 'email',
				value: user.email,
			});
			if (isEmailExist) {
				return {
					error: 'EMAIL_EXISTS',
					statusCode: 400,
					message: I18N('Email đã tồn tại'),
				};
			}

			const isUserNameExist = await this.userService.exists({
				key: 'username',
				value: user.phone,
			});
			if (isUserNameExist) {
				return {
					error: 'USERNAME_EXISTS',
					statusCode: 400,
					message: I18N('Tên tài khoản đã tồn tại'),
				};
			}

			const isPhoneExist = await this.userService.exists({
				key: 'phone',
				value: user.phone,
			});
			if (isPhoneExist) {
				return {
					error: 'PHONE_EXISTS',
					statusCode: 400,
					message: I18N('SĐT đã tồn tại'),
				};
			}

			const data = await this.userService.create(user);
			if (data.id) {
				return {
					data,
					statusCode: 200,
					message: I18N('Tạo tài khoản thành công'),
				};
			}
		} catch (err) {
			this.logger.error(err);
		}

		return {
			error: 'CANNOT_CREATE_USER',
			message: I18N('Không thể tạo tài khoản'),
			statusCode: 400,
		};
	}

	@Post('forgot')
	async forgot(@Body() body: ForgotBody): Promise<ResponseDto> {
		const { email } = body;
		const user = await this.userService.findOneByEmail(email);
		if (user && user.resetLimit <= 3) {
			const mail = await this.mailler.sendMailForgot(email);
			return mail;
		}

		return {
			statusCode: 400,
			message: I18N(
				'Email không tồn tại hoặc vượt quá số lần khôi phục, vui lòng liên hệ GM',
			),
		};
	}

	@Post('reset')
	async reset(@Body() body: ResetBody): Promise<ResponseDto> {
		const { password, email, token } = body;
		const user = await this.userService.findOneByEmail(email);
		if (
			user &&
			user.id &&
			token == user.resetPasswordToken &&
			dayjs().diff(dayjs.unix(user.resetPasswordExpires)) > 0
		) {
			const hashPassword = await bcrypt.hash(password, 10);
			const reset = await this.userService.password(
				user.id,
				hashPassword,
			);
			if (reset) {
				return {
					statusCode: 200,
					message: I18N('Khôi phục mật khẩu thành công'),
				};
			}
		}
		return {
			statusCode: 400,
			message: I18N('Khôi phục mật khẩu thất bại'),
		};
	}

	@Post('validate')
	async validate(@Body() body: ValidateBody): Promise<ResponseDto> {
		const { token } = body;

		try {
			const verify = await this.jwtService.verify(token);

			const user = await this.userService.validateUsername(
				verify.username,
			);
			if (user) {
				return {
					statusCode: 200,
					message: I18N('Token thành công'),
				};
			}

			return {
				statusCode: 400,
				error: 'TOKEN_INVALID',
				message: I18N('Token không có giá trị'),
			};
		} catch (err) {
			return {
				statusCode: 400,
				error: 'TOKEN_EXPIRED',
				message: I18N('Token đã hết hạn'),
			};
		}
	}

	@Post('verify-google-token')
	async verifyGoogleToken(
		@Body() body: VerifyGoogleTokenBody,
	): Promise<ResponseDto> {
		const { token } = body;
		try {
			const result = await this.httpService
				.get(
					`https://oauth2.googleapis.com/tokeninfo?id_token=${token}`,
				)
				.toPromise();
			const { email } = result.data;
			const user = await this.userService.findOneByEmail(email);
			if (!user) {
				const user = new User();
				user.email = email;
				user.username = email;
				user.password = '';
				user.roles = await this.roleService.findByName('MEMBER');
				const result = await this.userService.create(user);

				if (!result) {
					return {
						statusCode: 400,
						error: 'CANNOT_LOGIN_WITH_GOOGLE',
						message: I18N(
							'Không thể đăng nhập, vui lòng thử lại sau',
						),
					};
				} else {
					const payload = {
						username: user.username,
						phone: user.phone,
						email: user.email,
						isApp: true,
					};

					return {
						data: {
							token: this.jwtService.sign(payload),
							username: user.username,
							expiredAt: dayjs().add(30, 'days').unix(),
							email: user.email,
							phone: user.phone,
						},
						statusCode: 200,
						message: I18N('Đăng nhập thành công'),
					};
				}
			} else {
				const payload = {
					username: user.username,
					phone: user.phone,
					email: user.email,
					isApp: true,
				};

				return {
					data: {
						token: this.jwtService.sign(payload),
						username: user.username,
						expiredAt: dayjs().add(30, 'days').unix(),
						email: user.email,
						phone: user.phone,
					},
					statusCode: 200,
					message: I18N('Đăng nhập thành công'),
				};
			}
		} catch (err) {
			console.log(err);
			return {
				statusCode: 500,
				error: 'TOKEN_EXPIRED',
				message: I18N('Token đã hết hạn'),
			};
		}
	}

	@Throttle(1, 60)
	@Post('send-otp')
	async getPhoneSMSToken(@Body() body: SendOtpBody): Promise<ResponseDto> {
		try {
			const { phone } = body;
			const otp = Math.floor(100000 + Math.random() * 900000).toString();
			const otpExpires = dayjs().add(5, 'minutes').toDate();
			const user = await this.userService.findOneByPhone(phone);
			if (user) {
				const result = await this.userService.updateOtp(
					user.id,
					otp,
					otpExpires,
				);
				if (!result) {
					return {
						statusCode: 400,
						message: I18N('Không thể lưu OTP'),
					};
				}
				//otp service here
				// const send = await this.userService.sendOtp(phone, otp);

				// if (send) {
				// 	return {
				// 		statusCode: 200,
				// 		message: I18N('Gửi mã xác thực thành công!'),
				// 		data: {
				// 			otpExpires,
				// 			otp,
				// 		},
				// 	};
				// }
			} else {
				const user = new User();
				user.phone = phone;
				user.username = phone;
				user.password = '';
				user.roles = await this.roleService.findByName('MEMBER');
				user.count = 3;
				user.isActive = false;
				user.otpExpires = otpExpires;
				const result = await this.userService.create(user);

				if (!result) {
					return {
						statusCode: 400,
						message: I18N('Không thể lưu OTP'),
					};
				}

				// const send = await this.userService.sendOtp(phone, otp);
				// if (send) {
				// 	return {
				// 		statusCode: 200,
				// 		message: I18N('Gửi mã xác thực thành công!'),
				// 		data: {
				// 			otpExpires,
				// 			otp,
				// 		},
				// 	};
				// }
			}

			return {
				statusCode: 400,
				error: 'CANNOT_SEND_OTP',
				message: I18N(
					'Không thể gửi mã xác thực, vui lòng thử lại sau!',
				),
			};
		} catch (err) {
			console.log(err);
			return {
				statusCode: 500,
				error: 'WRONG_PHONE_NUMBER',
				message: I18N('Số điện thoại không đúng'),
			};
		}
	}

	@Post('verify-sms')
	async verifySMSToken(
		@Body() body: VerifyOtpBody,
	): Promise<ResponseDto<LoginResponseDto>> {
		try {
			const { otp, phone } = body;
			const user = await this.userService.findByOtp(otp, phone);
			if (user && dayjs().diff(user.otpExpires) < 0) {
				const payload = {
					username: user.username,
					phone: user.phone,
					email: user.email,
					isApp: true,
				};
				await this.userService.verifyPhone(user.id);
				return {
					data: {
						token: this.jwtService.sign(payload),
						username: user.username,
						expiredAt: dayjs().add(30, 'days').unix(),
						email: user.email,
						phone: user.phone,
					},
					statusCode: 200,
					message: I18N('Đăng nhập thành công'),
				};
			} else {
				return {
					statusCode: 400,
					error: 'CANNOT_VERIFY_OTP',
					message: I18N('Mã xác thực không đúng hoặc đã hết hạn'),
				};
			}
		} catch (err) {
			return {
				statusCode: 500,
				error: 'CANNOT_VERIFY_OTP',
				message: I18N('Mã xác thực không đúng hoặc đã hết hạn'),
			};
		}
	}
}
