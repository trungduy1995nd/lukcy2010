import { applyDecorators, createParamDecorator, ExecutionContext, SetMetadata } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';

export const Roles = (...roles: string[]) => {
	return applyDecorators(ApiBearerAuth(), SetMetadata('roles', roles));
};

export const CurrentUser = createParamDecorator((data: string, ctx: ExecutionContext) => {
	const request = ctx.switchToHttp().getRequest();
	const user = request.user;

	return data ? user && user[data] : user;
});
