import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { UserModule } from 'src/user/user.module';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './strategies/jwt.strategy';
import { HttpModule } from '@nestjs/axios';
import { RoleModule } from 'src/role/role.module';
import { ThrottlerModule } from '@nestjs/throttler';

@Module({
	providers: [AuthService, JwtStrategy],
	imports: [
		UserModule,
		PassportModule,
		RoleModule,
		JwtModule.register({
			secret: 'APP_SECRET',
			signOptions: { expiresIn: '30d' },
		}),
		HttpModule.register({
			timeout: 5000,
			maxRedirects: 5,
		}),
		ThrottlerModule.forRoot({
			ttl: 60,
			limit: 100,
		}),
	],
	controllers: [AuthController],
})
export class AuthModule {}
