import { ApiProperty } from '@nestjs/swagger';
import {
	IsEmail,
	IsNotEmpty,
	IsOptional,
	IsPhoneNumber,
	IsString,
	Length,
	MaxLength,
	MinLength,
} from 'class-validator';

export class LoginBody {
	@IsString()
	@IsNotEmpty()
	@MinLength(4)
	@MaxLength(12)
	@ApiProperty({ default: 'admin' })
	username: string;

	@IsString()
	@IsNotEmpty()
	@MinLength(6)
	@MaxLength(16)
	@ApiProperty({ default: 'admin!@#1234' })
	password: string;
}

export class LoginResponseDto {
	@ApiProperty()
	token: string;

	@ApiProperty()
	username: string;

	@ApiProperty()
	expiredAt: number;

	@ApiProperty()
	email: string;

	@ApiProperty()
	phone: string;

	@ApiProperty()
	@IsOptional()
	address?: string;
}
export class RegisterBody {
	@IsString()
	@IsNotEmpty()
	@MinLength(4)
	@MaxLength(12)
	@ApiProperty()
	username: string;

	@IsEmail()
	@IsNotEmpty()
	@ApiProperty()
	email: string;

	@IsString()
	@IsNotEmpty()
	@MinLength(6)
	@MaxLength(16)
	@ApiProperty()
	password: string;

	@IsPhoneNumber('VN')
	@IsNotEmpty()
	@ApiProperty()
	phone: string;
}

export class ForgotBody {
	@IsEmail()
	@IsNotEmpty()
	@ApiProperty()
	email: string;
}

export class ResetBody extends ForgotBody {
	@IsString()
	@MinLength(6)
	@MaxLength(16)
	@IsNotEmpty()
	@ApiProperty()
	password: string;

	@IsString()
	@ApiProperty()
	token: string;
}

export class ValidateBody {
	@IsString()
	@ApiProperty()
	token: string;
}

export class VerifyGoogleTokenBody {
	@IsString()
	@ApiProperty()
	token: string;
}

export class SendOtpBody {
	@ApiProperty()
	@IsPhoneNumber('VN')
	phone: string;
}

export class VerifyOtpBody {
	@IsString()
	@ApiProperty()
	@Length(6, 6)
	otp: string;

	@IsString()
	@ApiProperty()
	phone: string;
}

export class VerifyAppleToken {
	@IsString()
	@ApiProperty()
	code: string;

	@IsString()
	@ApiProperty({ default: 'IOS' })
	app: string;

	@IsString()
	@ApiProperty()
	token: string;
}
