import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserService } from 'src/user/user.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(private readonly userService: UserService) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: false,
			secretOrKey: process.env.SECRET_KEY,
		});
	}

	async validate(payload: any) {
		try {
			const user = await this.userService.validateUsername(
				payload.username,
			);
			if (!user.id) {
				throw new UnauthorizedException();
			}

			return user;
		} catch (error) {
			if (error) throw new UnauthorizedException();
		}
	}
}
