import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';
import { User } from '../entities/user.entity';
import * as bcrypt from 'bcrypt';

export default class CreateRoles implements Seeder {
	public async run(factory: Factory, connection: Connection): Promise<any> {
		const password = await bcrypt.hash('admin!@#1234', 10);
		await connection
			.createQueryBuilder()
			.insert()
			.into(User)
			.values([
				{
					username: 'admin',
					email: 'admin@gmail.com',
					phone: '0123456789',
					password: password,
				},
			])
			.execute();
	}
}
