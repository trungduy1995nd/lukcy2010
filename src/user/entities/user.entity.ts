import { Exclude, Expose } from 'class-transformer';
import { LuckyHistory } from 'src/lucky/entities/lucky-history.entity';
import { Role } from 'src/role/entities/role.entity';
import {
	Column,
	CreateDateColumn,
	Entity,
	Index,
	JoinTable,
	ManyToMany,
	OneToMany,
	PrimaryGeneratedColumn,
	Unique,
	UpdateDateColumn,
} from 'typeorm';

@Entity()
@Unique('UQ_username', ['username'])
@Unique('UQ_phone', ['phone'])
@Unique('UQ_email', ['email'])
export class User {
	@PrimaryGeneratedColumn({ unsigned: true })
	id: number;

	@Column({ type: 'varchar', length: 100 })
	username: string;

	@Column({ type: 'boolean', default: true })
	isActive = true;

	@Column({ type: 'boolean', default: false })
	isVerifyPhone = false;

	@Column({ type: 'varchar', nullable: true })
	phone: string;

	@Column({ type: 'varchar', nullable: true })
	email: string;

	@Column({ nullable: true })
	address: string;

	@Column({ type: 'varchar' })
	password: string;

	@ManyToMany(() => Role, (role) => role.user)
	@JoinTable({ name: 'users_roles' })
	roles: Role[];

	@Column()
	count: number;

	@Column({ nullable: true })
	otpExpires: Date;

	@Column({ type: 'varchar', default: '' })
	resetPasswordToken: string;

	@Column({ type: 'int', default: 0 })
	resetPasswordExpires: number;

	@Column({ type: 'int', default: 0 })
	resetLimit: number;

	@OneToMany(() => LuckyHistory, (lh) => lh.user)
	luckyHistories: LuckyHistory[];

	@CreateDateColumn()
	readonly createdAt: Date;

	@UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
	readonly updatedAt: Date;
}
