import { ApiProperty } from '@nestjs/swagger';
import {
	IsString,
	IsNotEmpty,
	MinLength,
	MaxLength,
	IsEmail,
	IsPhoneNumber,
	IsBoolean,
	IsNumber,
	IsOptional,
	IsNumberString,
} from 'class-validator';
import { Role } from 'src/role/entities/role.entity';

export class UpdateUserDto {
	@IsString()
	@IsNotEmpty()
	@MinLength(6)
	@MaxLength(12)
	@ApiProperty()
	username: string;

	@IsEmail()
	@IsNotEmpty()
	@ApiProperty()
	email: string;

	@IsString()
	@IsOptional()
	@ApiProperty()
	password: string;

	@IsString()
	@IsOptional()
	@ApiProperty()
	repassword: string;

	@IsPhoneNumber('VN')
	@IsNotEmpty()
	@ApiProperty()
	phone: string;

	@ApiProperty()
	@IsNumberString()
	isActive: boolean;

	@ApiProperty()
	@IsNumberString()
	roles: Role[];
}
