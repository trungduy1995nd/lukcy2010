import { FindQueryDto } from 'src/common/query.dto';

export class FindUserDto extends FindQueryDto {}
