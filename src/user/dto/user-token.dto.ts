import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class UserTokenDto {
	@ApiProperty()
	@IsString()
	token: string;
}
