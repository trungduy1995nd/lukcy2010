import { ApiProperty } from '@nestjs/swagger';
import {
	IsOptional,
	IsPhoneNumber,
	IsString,
	MaxLength,
	MinLength,
} from 'class-validator';

export class QueryUser {
	@ApiProperty()
	page: number;

	@ApiProperty()
	perPage: number;
}

export class ProfileDto {
	@IsOptional()
	@ApiProperty()
	firstName: string;

	@IsOptional()
	@ApiProperty()
	lastName: string;

	@IsOptional()
	@ApiProperty()
	avatar: string;

	@IsString()
	@ApiProperty()
	address: string;
}

export class PasswordDto {
	@IsString()
	@MinLength(6)
	@MaxLength(16)
	@ApiProperty()
	password: string;

	@IsString()
	@MinLength(6)
	@MaxLength(16)
	@ApiProperty()
	passwordNew: string;
}

export class AddPhoneDto {
	@ApiProperty()
	@IsString()
	@IsPhoneNumber('VN')
	phone: string;
}

export class VerifyPhoneDto {
	@ApiProperty()
	@IsString()
	otp: string;
}

export class AddEmailDto {
	@ApiProperty()
	@IsString()
	token: string;
}
