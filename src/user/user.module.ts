import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ThrottlerModule } from '@nestjs/throttler';
import { TypeOrmModule } from '@nestjs/typeorm';

import { User } from './entities/user.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
	providers: [UserService],
	exports: [UserService],
	imports: [
		TypeOrmModule.forFeature([User]),
		HttpModule.register({
			timeout: 5000,
			maxRedirects: 5,
		}),
		ThrottlerModule.forRoot({
			ttl: 60,
			limit: 100,
		}),
	],
	controllers: [UserController],
})
export class UserModule {}
