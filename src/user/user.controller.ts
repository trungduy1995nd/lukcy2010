import {
	Body,
	Controller,
	Get,
	Logger,
	Param,
	Patch,
	Post,
	Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import * as bcrypt from 'bcrypt';
import { CurrentUser, Roles } from 'src/auth/auth.meta';
import { ResponseDto } from 'src/common/response.dto';
import { Role } from 'src/role/entities/role.entity';
import { I18N } from 'src/utils/lang';

import { FindUserDto } from './dto/find-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PasswordDto, ProfileDto } from './dto/user.dto';
import { User } from './entities/user.entity';
import { UserService } from './user.service';

@ApiTags('User')
@Controller('user')
export class UserController {
	private readonly logger = new Logger(UserController.name);

	constructor(private readonly userService: UserService) {}

	@Roles('MEMBER')
	@Post('profile')
	async profile(
		@Body() body: ProfileDto,
		@CurrentUser() user: User,
	): Promise<ResponseDto> {
		try {
			const profile = await this.userService.update(user.id, body);
			if (profile.id) {
				return {
					message: I18N('Cập nhật thành công'),
					statusCode: 200,
				};
			}
		} catch (err) {
			this.logger.error(err);
		}

		return {
			message: I18N('Không thể cập nhật thông tin cá nhân'),
			statusCode: 400,
		};
	}

	@Roles('MEMBER')
	@Get('profile')
	async getProfile(@CurrentUser() user: User): Promise<ResponseDto> {
		const profile = await this.userService.findOne(user.id);
		return {
			statusCode: 200,
			data: profile,
		};
	}

	@Get(':id')
	@Roles('MEMBER')
	async findOne(@Param('id') id: string) {
		try {
			const result = await this.userService.findOne(+id);
			if (result) {
				return {
					data: result,
					statusCode: 200,
				};
			} else {
				return {
					message: I18N('Không tìm thấy dữ liệu'),
					statusCode: 404,
				};
			}
		} catch (error) {
			return {
				message: I18N('Không tìm thấy dữ liệu'),
				statusCode: 500,
				error: error.code,
			};
		}
	}

	@Roles('MEMBER')
	@Post('password')
	async password(
		@Body() body: PasswordDto,
		@CurrentUser() user: User,
	): Promise<ResponseDto> {
		try {
			const { password, passwordNew } = body;
			const isPasswordValid = await bcrypt.compare(
				password,
				user.password,
			);
			if (!isPasswordValid) {
				return {
					message: I18N('Mật khẩu hiện tại không đúng'),
					statusCode: 400,
				};
			}

			const hashPassword = await bcrypt.hash(passwordNew, 10);
			const change = await this.userService.password(
				user.id,
				hashPassword,
			);
			if (change.id) {
				return {
					message: I18N('Thay đổi mật khẩu thành công'),
					statusCode: 200,
				};
			}
		} catch (err) {
			this.logger.error(err);
		}
		return {
			message: I18N('Không thể thay đổi mật khẩu'),
			statusCode: 500,
		};
	}

	@Get()
	@Roles('ADMINISTRATOR')
	async findAll(@Query() query: FindUserDto): Promise<ResponseDto<User[]>> {
		try {
			const result = await this.userService.findAll(query);
			if (result.total) {
				return {
					statusCode: 200,
					...result,
					page: query.page,
					perPage: query.perPage,
				};
			} else {
				return {
					message: I18N('Không tìm thấy dữ liệu'),
					statusCode: 404,
					data: [],
				};
			}
		} catch (error) {
			return {
				message: I18N('Không tìm thấy dữ liệu'),
				statusCode: 400,
				error: error.code,
			};
		}
	}

	@Patch(':id')
	@Roles('ADMINISTRATOR')
	async update(
		@Param('id') id: string,
		@Body() updateUserDto: UpdateUserDto,
	) {
		try {
			const {
				password,
				repassword,
				email,
				isActive,
				phone,
				roles,
				username,
			} = updateUserDto;
			const user: UpdateUserDto = new UpdateUserDto();
			if (password) {
				user.password = await bcrypt.hash(password, 10);
			}

			if (password && password !== repassword) {
				return {
					message: I18N('Mật khẩu xác thực không khớp'),
					statusCode: 400,
				};
			}
			user.email = email;
			user.username = username;
			user.phone = phone;
			user.isActive = isActive;
			const role = new Role();
			role.id = Number(roles);
			user.roles = [role];
			const result = await this.userService.edit(+id, user);
			if (result.id) {
				return {
					message: I18N('Sửa thành viên thành công'),
					statusCode: 200,
					data: result,
				};
			} else {
				return {
					message: I18N('Sửa thành viên không thành công'),
					statusCode: 200,
				};
			}
		} catch (error) {
			console.log(error);
			return {
				message: I18N('Không thể sửa thành viên'),
				statusCode: 400,
				error: error.code,
			};
		}
	}
}
