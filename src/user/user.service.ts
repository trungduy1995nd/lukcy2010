import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import * as dayjs from 'dayjs';
import { Paginate } from 'src/common/api.interface';
import { makePagination } from 'src/utils/libs';
import { Repository } from 'typeorm';

import { FindUserDto } from './dto/find-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserTokenDto } from './dto/user-token.dto';
import { ProfileDto } from './dto/user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UserService {
	constructor(
		@InjectRepository(User) private userRepo: Repository<User>,
		private readonly configService: ConfigService,
		private readonly httpService: HttpService,
	) {}

	async findOne(id: number): Promise<User> {
		return this.userRepo.findOne(id, {
			relations: ['roles'],
			select: [
				'createdAt',
				'email',
				'id',
				'isActive',
				'isVerifyPhone',
				'phone',
				'updatedAt',
				'username',
			],
		});
	}

	async exists({ key, value }): Promise<boolean> {
		const count = await this.userRepo.count({ where: { [key]: value } });
		return count === 0 ? false : true;
	}

	async findAll(query: FindUserDto): Promise<Paginate> {
		const { skip, order, take } = makePagination(query);

		const [result, total] = await this.userRepo.findAndCount({
			relations: ['roles'],
			order: order,
			take: take,
			skip: skip,
			select: [
				'createdAt',
				'email',
				'id',
				'isActive',
				'isVerifyPhone',
				'phone',
				'updatedAt',
				'username',
			],
		});

		return {
			data: result,
			total: total,
		};
	}

	async remove(id: number): Promise<void> {
		await this.userRepo.delete(id);
	}

	async create(user: User): Promise<User> {
		return this.userRepo.save(user);
	}

	async findOneByPhone(phone: string): Promise<User> {
		return this.userRepo.findOne({
			where: { phone },
		});
	}

	async findByOtp(otp: string, phone: string): Promise<User> {
		return this.userRepo.findOne({
			where: { otp, phone },
		});
	}

	async validateUsername(username: string): Promise<User> {
		return this.userRepo.findOne({
			where: { username },
			relations: ['roles', 'roles.permissions'],
		});
	}

	async findOneByEmail(email: string): Promise<User> {
		return this.userRepo.findOne({
			where: { email },
		});
	}

	async update(id: number, data: ProfileDto): Promise<User> {
		return this.userRepo.save({ id, ...data });
	}

	async edit(id: number, data: UpdateUserDto): Promise<User> {
		return this.userRepo.save({ id, ...data });
	}

	async password(id: number, password: string): Promise<User> {
		return this.userRepo.save({
			id,
			password,
			resetPasswordToken: '',
			resetPasswordExpires: 0,
			resetLimit: 0,
		});
	}

	async generateForgotToken(email: string): Promise<User> {
		const user = await this.userRepo.findOne({
			where: { email },
		});

		if (user && user.id) {
			const resetPasswordToken = Math.floor(
				Math.random() * (99999 - 10000) + 10000,
			).toString();
			user.resetPasswordToken = resetPasswordToken;
			const resetPasswordExpires = dayjs().add(5).unix();
			await this.userRepo.update(
				{ id: user.id },
				{
					resetPasswordToken,
					resetPasswordExpires,
					resetLimit: () => `resetLimit + 1`,
				},
			);
			return user;
		}

		return null;
	}

	async updateOtp(id: number, otp: string, otpExpires: Date): Promise<User> {
		return this.userRepo.save({ id, otp, otpExpires });
	}

	async updateEmail(id: number, email: string): Promise<User> {
		return this.userRepo.save({ id, email });
	}

	async updatePhone(
		id: number,
		phone: string,
		otp: string,
		otpExpires: Date,
	): Promise<User> {
		return this.userRepo.save({ id, phone, otp, otpExpires });
	}

	async verifyPhone(id: number): Promise<User> {
		return this.userRepo.save({ id, isVerifyPhone: true });
	}

	async removePhone(id: number): Promise<User> {
		return this.userRepo.save({ id, phone: null });
	}

	async count(): Promise<number> {
		return this.userRepo.count();
	}

	async decreaseCount(id: number, count: number) {
		return this.userRepo.decrement({ id }, 'count', count);
	}

	async increaseCount(id: number, count: number) {
		return this.userRepo.increment({ id }, 'count', count);
	}
}
