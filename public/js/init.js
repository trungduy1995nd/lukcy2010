$(document).ready(function () {
	//  Setup variables
	var wheel = $('.wheel'),
		active = $('.active'),
		currentRotation,
		lastRotation = 0,
		tolerance,
		deg,
		$btnPlay = $('#btnPlay'),
		$btnSlowMo = $('#btnSlowMo');

	//  Random degree
	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	window.playing = false;
	var audio = document.getElementById('playAudio');
	var indicator = new TimelineMax();
	var spinWheel = new TimelineMax();
	indicator
		.to(active, 0.13, {
			rotation: -10,
			transformOrigin: '65% 36%',
			ease: Power1.easeOut,
		})
		.to(active, 0.13, {
			rotation: 3,
			ease: Power4.easeOut,
		})
		.add('end');

	$btnPlay.click(function () {
		if (!playing) {
			const token = localStorage.getItem('TOKEN');
			$('#btnPlay').removeClass('init');
			if (token) {
				window.playing = true;

				fetch(`http://tidx.ddns.net:3000/api/v1/spin`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${token}`,
					},
				})
					.then((result) => {
						return result.json();
					})
					.then((response) => {
						if (response.statusCode === 200) {
							const data = response.data;
							//  Creating the Timeline
							const ele = $('[data-id="' + data.id + '"]').data(
								'index',
							);
							console.log(ele, data.id);
							const winner = 15 + Math.abs(ele - 14) * 30 + 1080;
							audio.currentTime = 0;
							audio.play();
							//   Buttons
							spinWheel.to(wheel, 5, {
								rotation: winner,
								transformOrigin: '50% 50%',
								ease: Power4.easeOut,
								onUpdate: function () {
									currentRotation = Math.round(
										this.target[0]._gsTransform.rotation,
									); //_gsTransform: current position of the wheel
									tolerance = currentRotation - lastRotation;
									if (
										Math.round(currentRotation) %
											(360 / 12) <=
										tolerance
									) {
										if (
											indicator.progress() > 0.2 ||
											indicator.progress() === 0
										) {
											indicator.play(0);
										}
									}
									lastRotation = currentRotation;
								},
								onComplete: function () {
									swal({
										text: '',
										icon: 'success',
										title: `${data.name}`,
										button: {
											text: 'Thank you~',
											closeModal: false,
										},
									}).then(() => {
										//
										spinWheel.set(wheel, {
											rotation: 0,
										});
										window.playing = false;
										swal.stopLoading();
										swal.close();
									});
								},
							});
						} else {
							swal({
								title: 'Out of time',
								icon: 'error',
								text: `cascacs`,
								button: {
									text: 'Thank you~',
									closeModal: false,
								},
							}).then(() => {
								//
								spinWheel.set(wheel, {
									rotation: 0,
								});
								window.playing = false;
								swal.stopLoading();
								swal.close();
							});
						}
					})
					.catch((err) => {
						//
					});

				// indicator.timeScale(1).seek(0);
				// spinWheel.timeScale(1).seek(0);
			} else {
				loginModal();
			}
		}
	});

	fetch(`http://tidx.ddns.net:3000/api/v1/gifts`)
		.then((result) => {
			return result.json();
		})
		.then((response) => {
			if (response.statusCode === 200) {
				const data = response.data;
				for (let i = 0; i < 12; i++) {
					const id = i + 1;
					$('#img_' + id).attr('xlink:href', data[i].image);
					$('#img_' + id).attr('data-id', data[i].id);
					$('#img_' + id).attr('data-index', i);
				}
			}
		})
		.catch((err) => {
			//
		});
});
