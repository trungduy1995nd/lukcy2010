$(document).ready(function () {
	window.loginModal = function () {
		swal({
			text: "Enter your FSOFT's account",
			content: 'input',
			closeOnClickOutside: false,
			closeOnEsc: false,

			button: {
				text: 'Next~',
				closeModal: false,
			},
		})
			.then((name) => {
				if (!name) throw null;

				return fetch(`http://tidx.ddns.net:3000/api/v1/auth/login`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify({
						username: name,
						password: '123456',
					}),
				});
			})
			.then((results) => {
				return results.json();
			})
			.then((json) => {
				console.log(json);

				if (json.statusCode !== 200) {
					return swal(json.message);
				}
				const data = json.data;
				localStorage.setItem('TOKEN', data.token);
				localStorage.setItem('USERNAME', data.username);
				if (data.address) {
					swal.stopLoading();
					swal.close();
					return;
				}
				swal({
					text: 'Enter your address',
					content: 'input',
					closeOnClickOutside: false,
					closeOnEsc: false,

					button: {
						text: 'Play!!!!',
						closeModal: false,
					},
				})
					.then((address) => {
						if (!address) throw null;

						return fetch(
							`http://tidx.ddns.net:3000/api/v1/user/profile`,
							{
								method: 'POST',
								headers: {
									'Content-Type': 'application/json',
									Authorization: `Bearer ${data.token}`,
								},
								body: JSON.stringify({
									address: address,
								}),
							},
						);
					})
					.then(() => {
						swal.stopLoading();
						swal.close();
					});
			})
			.catch((err) => {
				console.log(err);
				if (err) {
					swal('Oh noes!', 'The AJAX request failed!', 'error');
				} else {
					swal.stopLoading();
					swal.close();
				}
			});
	};
});
