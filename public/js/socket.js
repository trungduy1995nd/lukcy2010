const socket = io('http://tidx.ddns.net:3000');

const messages = document.getElementById('incommingMessage');

socket.on('message', ({ username, message }) => {
	handleNewMessage(username, message);
});

const handleNewMessage = (username, message) => {
	messages.appendChild(buildNewMessage(username, message));
	$('.incoming').animate({ scrollTop: 99999 }, 'slow');
};

const buildNewMessage = (username, message) => {
	const div = document.createElement('div');
	div.className = 'bubble';
	div.innerHTML = `<span class="name">${username}</span> đã quay trúng
	${message}`;
	return div;
};
