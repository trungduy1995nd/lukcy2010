module.exports = {
	apps: [
		{
			name: 'Lucky',
			script: 'dist/main.js',
			watch: false,
			instances: 1,
			env: {
				NODE_ENV: 'development',
			},
			env_production: {
				NODE_ENV: 'production',
				PORT: 3001,
			},
		},
	],
};
